﻿namespace SMS2Device
{
    partial class App
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblMessage = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.LB_Log = new System.Windows.Forms.ListBox();
            this.btn_Start = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAuthenCode = new System.Windows.Forms.TextBox();
            this.txtApiHash = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnAuthenCode = new System.Windows.Forms.Button();
            this.txtApiID = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btn_Stop = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblMessage
            // 
            this.lblMessage.BackColor = System.Drawing.Color.SandyBrown;
            this.lblMessage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblMessage.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblMessage.ForeColor = System.Drawing.Color.Maroon;
            this.lblMessage.Location = new System.Drawing.Point(0, 420);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(350, 67);
            this.lblMessage.TabIndex = 13;
            this.lblMessage.Text = "....";
            this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer1
            // 
            this.timer1.Interval = 2000;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.SandyBrown;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Maroon;
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(350, 37);
            this.label5.TabIndex = 16;
            this.label5.Text = "Tự động gửi SMS đến di động bằng telegram";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 37);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(350, 383);
            this.tabControl1.TabIndex = 17;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.LB_Log);
            this.tabPage3.Controls.Add(this.btn_Stop);
            this.tabPage3.Controls.Add(this.btn_Start);
            this.tabPage3.Location = new System.Drawing.Point(4, 23);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(342, 356);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Tự động";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // LB_Log
            // 
            this.LB_Log.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LB_Log.FormattingEnabled = true;
            this.LB_Log.ItemHeight = 14;
            this.LB_Log.Location = new System.Drawing.Point(3, 69);
            this.LB_Log.Name = "LB_Log";
            this.LB_Log.Size = new System.Drawing.Size(336, 284);
            this.LB_Log.TabIndex = 339;
            // 
            // btn_Start
            // 
            this.btn_Start.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btn_Start.FlatAppearance.BorderSize = 0;
            this.btn_Start.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Start.Location = new System.Drawing.Point(45, 6);
            this.btn_Start.Name = "btn_Start";
            this.btn_Start.Size = new System.Drawing.Size(120, 54);
            this.btn_Start.TabIndex = 1;
            this.btn_Start.Text = "Chạy tự động";
            this.btn_Start.UseVisualStyleBackColor = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(342, 356);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Cấu hình";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtPhone);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.txtAuthenCode);
            this.panel2.Controls.Add(this.txtApiHash);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.btnConnect);
            this.panel2.Controls.Add(this.btnAuthenCode);
            this.panel2.Controls.Add(this.txtApiID);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(336, 350);
            this.panel2.TabIndex = 17;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(52, 36);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(235, 22);
            this.txtPhone.TabIndex = 10;
            this.txtPhone.Text = "+84902106212";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(49, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 14);
            this.label2.TabIndex = 11;
            this.label2.Text = "ApiID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(49, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "Số điện thoại dùng gửi SMS";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(49, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 14);
            this.label4.TabIndex = 11;
            this.label4.Text = "ApiHash";
            // 
            // txtAuthenCode
            // 
            this.txtAuthenCode.Location = new System.Drawing.Point(52, 248);
            this.txtAuthenCode.Margin = new System.Windows.Forms.Padding(4);
            this.txtAuthenCode.Name = "txtAuthenCode";
            this.txtAuthenCode.PasswordChar = '*';
            this.txtAuthenCode.Size = new System.Drawing.Size(235, 22);
            this.txtAuthenCode.TabIndex = 8;
            // 
            // txtApiHash
            // 
            this.txtApiHash.Location = new System.Drawing.Point(52, 136);
            this.txtApiHash.Name = "txtApiHash";
            this.txtApiHash.PasswordChar = '*';
            this.txtApiHash.Size = new System.Drawing.Size(235, 22);
            this.txtApiHash.TabIndex = 10;
            this.txtApiHash.Text = "0f6416e58a5dcb9eb279629a57c041d3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(49, 226);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 14);
            this.label3.TabIndex = 9;
            this.label3.Text = "Mã xác thực";
            // 
            // btnConnect
            // 
            this.btnConnect.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnConnect.FlatAppearance.BorderSize = 0;
            this.btnConnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConnect.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnConnect.Location = new System.Drawing.Point(91, 163);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(150, 41);
            this.btnConnect.TabIndex = 12;
            this.btnConnect.Text = "Kết nối Telegram";
            this.btnConnect.UseVisualStyleBackColor = false;
            // 
            // btnAuthenCode
            // 
            this.btnAuthenCode.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnAuthenCode.FlatAppearance.BorderSize = 0;
            this.btnAuthenCode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAuthenCode.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnAuthenCode.Location = new System.Drawing.Point(91, 276);
            this.btnAuthenCode.Name = "btnAuthenCode";
            this.btnAuthenCode.Size = new System.Drawing.Size(150, 41);
            this.btnAuthenCode.TabIndex = 12;
            this.btnAuthenCode.Text = "Xác thực Phone";
            this.btnAuthenCode.UseVisualStyleBackColor = false;
            // 
            // txtApiID
            // 
            this.txtApiID.Location = new System.Drawing.Point(52, 86);
            this.txtApiID.Name = "txtApiID";
            this.txtApiID.PasswordChar = '*';
            this.txtApiID.Size = new System.Drawing.Size(235, 22);
            this.txtApiID.TabIndex = 10;
            this.txtApiID.Text = "5022342";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.PowderBlue;
            this.label6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(0, 487);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(350, 19);
            this.label6.TabIndex = 18;
            this.label6.Text = "2021 © TNS. All rights reserved.";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_Stop
            // 
            this.btn_Stop.BackColor = System.Drawing.Color.Orange;
            this.btn_Stop.FlatAppearance.BorderColor = System.Drawing.Color.Orange;
            this.btn_Stop.FlatAppearance.BorderSize = 0;
            this.btn_Stop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Stop.Location = new System.Drawing.Point(171, 6);
            this.btn_Stop.Name = "btn_Stop";
            this.btn_Stop.Size = new System.Drawing.Size(120, 54);
            this.btn_Stop.TabIndex = 1;
            this.btn_Stop.Text = "Dừng";
            this.btn_Stop.UseVisualStyleBackColor = false;
            // 
            // App
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 506);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.label6);
            this.Font = new System.Drawing.Font("Tahoma", 9F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "App";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.App_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAuthenCode;
        private System.Windows.Forms.TextBox txtApiHash;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnAuthenCode;
        private System.Windows.Forms.TextBox txtApiID;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ListBox LB_Log;
        private System.Windows.Forms.Button btn_Start;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_Stop;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TeleSharp.TL;
using TeleSharp.TL.Contacts;
using TLSharp.Core;

namespace SMS2Device
{
    public partial class App : Form
    {
        //int _ApiID = 5022342;
        //string _ApiHash = "0f6416e58a5dcb9eb279629a57c041d3";
        //string _MyPhone = "+84902106212";
        string _AuthenHash = "";
        string _UserSelected = "";
        string _SessionPath = "";
        private string _Path = Directory.GetCurrentDirectory() + "\\Info.txt";
        TelegramClient _Tele;
        TLContacts tLContact;
        string _Log;
        public App()
        {
            InitializeComponent();
            timer1.Tick += Timer1_Tick;
            btn_Start.Click += Btn_Start_Click;
            btn_Stop.Click += Btn_Stop_Click;
            btnConnect.Click += BtnConnect_Click;
            btnAuthenCode.Click += btnAuthenCode_Click;
            if (File.Exists("session.dat"))
            {
                _SessionPath = Directory.GetCurrentDirectory() + "\\session";
            }
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();

            var zList = new List<Task_Model>();

            DataTable zTable = new DataTable();
            string zSQL = "SELECT B.*, A.AutoKey, A.SentGuest, A.SentStaff FROM CRM_Task_SMS A LEFT JOIN CRM_Task B ON A.TaskKey = B.TaskKey WHERE SentGuest = 0 OR SentStaff = 0";
            string zConnectionString = "server = anbinhjsc.com.vn; database = TN_ERP_V01;uid = thanhtam;password = Abc!@#789;";
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
               
                foreach (DataRow r in zTable.Rows)
                {

                    zList.Add(new Task_Model()
                    {
                        SentStaff = (r["SentStaff"] == DBNull.Value ? false : bool.Parse(r["SentStaff"].ToString())),
                        SentGuest = (r["SentGuest"] == DBNull.Value ? false : bool.Parse(r["SentGuest"].ToString())),

                        TaskKey = r["TaskKey"].ToString(),
                        TaskFile = r["TaskFile"].ToString(),
                        Subject = r["Subject"].ToString(),
                        TaskContent = r["TaskContent"].ToString(),
                        StartDate = (r["StartDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["StartDate"]),
                        DueDate = (r["DueDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DueDate"]),
                        Duration = (r["Duration"] == DBNull.Value ? 0 : float.Parse(r["Duration"].ToString())),
                        StatusKey = (r["StatusKey"] == DBNull.Value ? 0 : int.Parse(r["StatusKey"].ToString())),
                        StatusName = r["StatusName"].ToString(),
                        PriorityKey = (r["PriorityKey"] == DBNull.Value ? 0 : int.Parse(r["PriorityKey"].ToString())),
                        PriorityName = r["PriorityName"].ToString(),
                        CompleteRate = (r["CompleteRate"] == DBNull.Value ? 0 : int.Parse(r["CompleteRate"].ToString())),
                        CompleteDate = (r["CompleteDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CompleteDate"]),
                        CategoryKey = (r["CategoryKey"] == DBNull.Value ? 0 : int.Parse(r["CategoryKey"].ToString())),
                        CategoryName = r["CategoryName"].ToString(),
                        GroupKey = (r["GroupKey"] == DBNull.Value ? 0 : int.Parse(r["GroupKey"].ToString())),
                        GroupName = r["GroupName"].ToString(),
                        ParentKey = r["ParentKey"].ToString(),
                        CustomerKey = r["CustomerKey"].ToString(),
                        CustomerID = r["CustomerID"].ToString(),
                        CustomerName = r["CustomerName"].ToString(),
                        CustomerPhone = r["CustomerPhone"].ToString(),
                        CustomerAddress = r["CustomerAddress"].ToString(),
                        ContractKey = r["ContractKey"].ToString(),
                        ContractName = r["ContractName"].ToString(),
                        Reminder = (r["Reminder"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["Reminder"]),
                        ApproveBy = r["ApproveBy"].ToString(),
                        ApproveName = r["ApproveName"].ToString(),
                        OwnerBy = r["OwnerBy"].ToString(),
                        OwnerName = r["OwnerName"].ToString(),
                        Style = r["Style"].ToString(),
                        Class = r["Class"].ToString(),
                        CodeLine = r["CodeLine"].ToString(),
                        Slug = (r["Slug"] == DBNull.Value ? 0 : int.Parse(r["Slug"].ToString())),
                        RecordStatus = (r["RecordStatus"] == DBNull.Value ? 0 : int.Parse(r["RecordStatus"].ToString())),
                        Publish = (r["Publish"] == DBNull.Value ? false : bool.Parse(r["Publish"].ToString())),
                        PartnerNumber = r["PartnerNumber"].ToString(),
                        DepartmentKey = r["DepartmentKey"].ToString(),
                        BranchKey = r["BranchKey"].ToString(),
                        OrganizationID = r["OrganizationID"].ToString(),
                        CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                        CreatedBy = r["CreatedBy"].ToString(),
                        CreatedName = r["CreatedName"].ToString(),
                        ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                        ModifiedBy = r["ModifiedBy"].ToString(),
                        ModifiedName = r["ModifiedName"].ToString(),
                        OwnerPhone = r["OwnerPhone"].ToString(),
                        AutoKey = int.Parse(r["AutoKey"].ToString()),
                    });
                }
            }
            catch (Exception)
            {

            }

            if (zList.Count > 0)
            {
                foreach (var item in zList)
                {

                    if (item.OwnerPhone.Length >= 9)
                    {
                        string phoneStaff = "+84" + item.OwnerPhone.Substring(1, item.OwnerPhone.Length - 1);
                        AddLog("Đang gửi tin nhắn đến: " + phoneStaff + "");
                        AddLog("                   -----                  ");
                        //Tele_Send(phoneStaff, "TNSOLUTION nhân viên-Tin nhắn tự động");

                         zSQL = "UPDATE  CRM_Task_SMS SET SentGuest = 1 , SentStaff = 1 WHERE AutoKey= "+item.AutoKey;
                         zConnectionString = "server = anbinhjsc.com.vn; database = TN_ERP_V01;uid = thanhtam;password = Abc!@#789;";
                        try
                        {
                            SqlConnection zConnect = new SqlConnection(zConnectionString);
                            zConnect.Open();
                            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                            zCommand.ExecuteNonQuery();
                            zCommand.Dispose();
                            zConnect.Close();
                            AddLog("Đã gửi tin nhắn đến: " + phoneStaff + "");
                        }
                        catch
                        {
                            AddLog("Lỗi. gửi tin nhắn đến: " + phoneStaff + "");
                        }
                           
                        AddLog("/--------------------/---------------------/");
                    }
                    if (item.OwnerPhone.Length >= 9)
                    {
                        string phoneGuest = "+84" + item.CustomerPhone.Substring(1, item.OwnerPhone.Length - 1);
                        AddLog("Đang gửi tin nhắn đến: " + phoneGuest + "");
                        AddLog("                   -----                  ");
                        //Tele_Send(phoneGuest, "TNSOLUTION khách hàng-Tin nhắn tự động");
                        zSQL = "UPDATE  CRM_Task_SMS SET SentGuest = 1 , SentStaff = 1 WHERE AutoKey= " + item.AutoKey;
                        zConnectionString = "server = anbinhjsc.com.vn; database = TN_ERP_V01;uid = thanhtam;password = Abc!@#789;";
                        try
                        {
                            SqlConnection zConnect = new SqlConnection(zConnectionString);
                            zConnect.Open();
                            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                            zCommand.ExecuteNonQuery();
                            zCommand.Dispose();
                            zConnect.Close();
                            AddLog("Đã gửi tin nhắn đến: " + phoneGuest + "");
                        }
                        catch(Exception ex)
                        {
                            AddLog("Lỗi. gửi tin nhắn đến: " + phoneGuest + "");
                        }
                        AddLog("Đã gửi tin nhắn đến: " + phoneGuest + "");
                        AddLog("/--------------------/---------------------/");
                    }
                    
                }
            }

            timer1.Start();
        }

        private async void App_Load(object sender, EventArgs e)
        {
            if (_SessionPath.Length > 0)
            {
                var store = new FileSessionStore();
                _Tele = new TelegramClient(int.Parse(txtApiID.Text), txtApiHash.Text, store, _SessionPath);
                await _Tele.ConnectAsync(false);

                if (_Tele.IsUserAuthorized())
                {
                    lblMessage.Text = "Đã kết nối !.";
                }
            }
            else
            {
                _Tele = new TelegramClient(int.Parse(txtApiID.Text), txtApiHash.Text);
            }

            if (!_Tele.IsConnected)
            {
                lblMessage.Text = "Chưa kết nối đến Telegram. Vui lòng thực hiện kết nối và xác thực số điện thoại !.";
            }
            else
            {
                lblMessage.Text = "Đã kết nối !.";
            }

            //string[] zText = File.ReadAllText(_Path).ToString().Replace("\r\n", " ").Split(' ');
            //if (zText.Length > 0)
            //{
            //    ListView LV = LV_Info;
            //    ListViewItem lvi;
            //    ListViewItem.ListViewSubItem lvsi;
            //    LV.Items.Clear();
            //    for (int i = 0; i < zText.Length - 1; i++)
            //    {
            //        string[] temp = zText[i].Split(';');
            //        lvi = new ListViewItem();
            //        lvi.Text = temp[0].Trim();

            //        lvsi = new ListViewItem.ListViewSubItem();
            //        lvsi.Text = temp[1].Trim();
            //        lvi.SubItems.Add(lvsi);

            //        lvsi = new ListViewItem.ListViewSubItem();
            //        lvsi.Text = temp[2].Trim();
            //        lvi.SubItems.Add(lvsi);
            //        LV.Items.Add(lvi);
            //    }
            //}
        }
        //private void Btn_Save_Click(object sender, EventArgs e)
        //{
        //    StringBuilder zSb = new StringBuilder();

        //    foreach (ListViewItem item in LV_Info.Items)
        //    {
        //        string zPhone = item.SubItems[0].Text.ToString();
        //        string zApiID = item.SubItems[1].Text.ToString(); ;
        //        string zApiHash = item.SubItems[2].Text.ToString();

        //        zSb.AppendLine(zPhone + ";" + zApiID + ";" + zApiHash);

        //    }
        //    File.WriteAllText(_Path, zSb.ToString());
        //    MessageBox.Show("Ghi nhớ thành công!");
        //}
        
        private void Btn_Stop_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            AddLog("                         ");
            AddLog("----" + DateTime.Now + "----");
            AddLog("--Dừng gửi tin nhắn--");

        }
        private void Btn_Start_Click(object sender, EventArgs e)
        {
            timer1.Start();
            timer1.Enabled = true;
            LB_Log.Items.Clear();
            AddLog("                         ");
            AddLog("--Bắt đầu gửi tin nhắn--");
            AddLog("----"+DateTime.Now+"----");
            AddLog("                         ");
        }
        private async void BtnConnect_Click(object sender, EventArgs e)
        {
            if (txtPhone.Text != "" && txtPhone.Text.Length >= 9)
            {
                if (txtApiHash.Text.Length > 0 && txtApiID.Text.Length >0)
                {
                    await _Tele.ConnectAsync(false);
                    _AuthenHash = await _Tele.SendCodeRequestAsync(txtPhone.Text);
                    lblMessage.Text = "Mã xác thực đã gửi.Vui lòng kiểm tra tin nhắn điện thoại!";
                }
                else
                {
                    lblMessage.Text = "Vui lòng nhập ApiID và ApiHash!";
                }
            }
            else
            {
                lblMessage.Text = "Vui lòng nhập số điện thoại!";
            }
        }
        private async void btnAuthenCode_Click(object sender, EventArgs e)
        {
            if (txtAuthenCode.Text.Length > 0)
            {
                if (txtApiID.Text.Length > 0 && txtPhone.Text.Length >= 9)
                {
                    try
                    {
                        await _Tele.MakeAuthAsync(txtPhone.Text, _AuthenHash, txtAuthenCode.Text);
                        tLContact = await _Tele.GetContactsAsync();
                        lblMessage.Text = "Đã kết nối!";
                    }
                    catch
                    {
                        lblMessage.Text = "Mã xác thực không đúng!";
                    }
                }
                else
                {
                    lblMessage.Text = "Vui lòng nhập đầy đủ thông tin!";
                }
            }
            else
            {
                lblMessage.Text = "Vui lòng nhập mã xác thực!";
            }
        }



        private async void Tele_Send(string phone, string Message)
        {
            var user = tLContact.Users
              .Where(x => x.GetType() == typeof(TLUser))
              .Cast<TLUser>()
              .FirstOrDefault(x => x.Phone == phone);

            await _Tele.SendMessageAsync(new TLInputPeerUser() { UserId = user.Id }, Message);
        }

        void AddLog(string Text)
        {
            Invoke(new MethodInvoker(delegate
            {
                LB_Log.SelectedIndex = LB_Log.Items.Count - 1;
                LB_Log.Items.Add(DateTime.Now.ToString("hh:mm:ss.fff") + " : " + Text);

            }));
        }

    }
}
